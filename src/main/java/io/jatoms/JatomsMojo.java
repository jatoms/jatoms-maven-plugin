package io.jatoms;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import io.jatoms.model.Jatoms;
import io.jatoms.model.JatomsContext;
import io.jatoms.util.DOMHelper;
import io.jatoms.util.JatomsHelper;

public abstract class JatomsMojo extends AbstractMojo {

    protected Log log;
    protected DOMHelper dom;
    protected Jatoms jatoms;
    protected Path baseDirPath;
    protected JatomsHelper helper;
    protected JatomsContext context;

    protected void init(String coordinates, String repository, String context) throws MojoExecutionException {
        log = getLog();

        log.info("\tINIT");
        dom = new DOMHelper();
        jatoms = parseJatoms(coordinates, repository);


        log.info("\t\tCreating Jatoms Helper");
        helper = new JatomsHelper(log, jatoms);
        baseDirPath = Paths.get(".").toAbsolutePath().normalize();
        log.info("\t\tSetting base directory path to " + baseDirPath.toString());

        // parse context
        if(context != null){
            log.info("\t\tContextfile given: " + context);
            Path contextPath = baseDirPath.resolve(context);
            if(contextPath.toFile().exists()){
                this.context = helper.parseContext(contextPath);
            } else {
                log.warn("\t\tThere was no context file at " + contextPath + ". Proceeding with default context.");
            }
        } 
        // Fallback context object
        if(this.context == null) {
            this.context = new JatomsContext();
        }

        log.info("\t\tAdding JATOMS object to context");
        this.context.fields.put("JATOMS", jatoms.toMap());
    }

    private Jatoms parseJatoms(String coordinates, String repository) throws MojoExecutionException {
        Jatoms jatoms = new Jatoms();
        jatoms.coordinates = coordinates;
        jatoms.repository = repository;

        log.info("\t\tCreating JATOMS object");
        log.info("\t\t\tParsing coordinates: " + coordinates);
        String[] cparts = coordinates.split(":");

        if(cparts.length != 3){
            throw new MojoExecutionException("Coordinates must be in the form 'groupId:artifactId:version'");
        }

        String groupId = cparts[0];
        String artifactId = cparts[1];
        String version = cparts[2];

        jatoms.groupId = groupId;
        jatoms.artifactId = artifactId;
        jatoms.version = version;
        log.info("\t\t\tCreated new properties int JATOMS object: groupId/artifactId/version = " + groupId + "/"
                + artifactId + "/" + version);

        // parse package
        String groupPath = groupId.replace(".", File.separator).toLowerCase();
        String artifactPath = artifactId.replace("-", File.separator).replace("_", File.separator).toLowerCase();
        Path packagePath = null; 
        for (String segment : groupPath.split(File.separator)) {
            packagePath = (packagePath == null) ? Paths.get(segment) : packagePath.resolve(segment);
        }
        for(String segment : artifactPath.split(File.separator)) {
            packagePath = packagePath.resolve(segment);
        }
        String packageString = packagePath.toString().replace(File.separator, ".");

        jatoms.packagePath = packagePath;
        jatoms.packageString = packageString;

        log.info("\t\t\tCreated new properties int JATOMS object: packagePath :: package = " + packagePath.toString() + " :: "
                + packageString);
        return jatoms;
    }

}