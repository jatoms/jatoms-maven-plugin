package io.jatoms;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.w3c.dom.Document;

import io.jatoms.util.FeatureMerger;
import io.jatoms.util.POMHelper;
import io.jatoms.util.POMMerger;

@Mojo(name = "submodule", defaultPhase = LifecyclePhase.PROCESS_SOURCES, requiresProject = false)
public class SubmoduleMojo extends JatomsMojo {

    @Parameter(defaultValue = "jatoms.json", property = "jcontext", required = false)
    private String _context;

    @Parameter(property = "jrepo", required = true)
    private String _repository;

    @Parameter(property = "jcoords", required = true)
    private String _coordinates;

    @Parameter(property = "jfeature", required = false, defaultValue = "featurerepo/src/main/feature/feature.xml")
    private String _featurerepo;

    // path to pom in folder from which plugin was executed
    private Path pomPath;
    // path to root pom, i.e., the upmost pom when going into upwards directories from pom
    private Path rootPomPath;
    // path to submodule folder
    private Path submodulePath;
    // path to mixin pom that comes with the submodule template
    private Path mixinPomPath;
    // path to pom of the submodule
    private Path submodulePomPath;
    //path to featurerepo
    private Path featurerepoPath;
    //path to featurerepo mixin
    private Path mixinFeaturerepoPath;;

    // helper for operations on the pom
    private POMHelper pom;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        init(_coordinates, _repository, _context);

        // copy template inot subfolder
        helper.fetchTemplate(submodulePath);
        helper.parseDefaultContext(submodulePath, context);
        validateTemplate();
        
        log.info("\tEXECUTE");
        // add new submodule to current pom
        addSubmoduleToPom();
        // add parent to submodule pom
        addParentToSubmodulePom();
        // merge mixinpom.xml and actual root pom
        mergeMixinPomIntoRootPom();
        // merge mixinFeaturepo and actual featurerepo
        mergeMixinFeaturerepoIntoFeaturerepo();

        // process all templates in subfolder
        helper.processTemplate(context, submodulePath);
    }
    

    @Override
    protected void init(String coordinates, String repository, String context) throws MojoExecutionException {
        super.init(coordinates, repository, context);
        this.pom = new POMHelper(log, dom, jatoms);
        pomPath = baseDirPath.resolve("pom.xml");
        rootPomPath = pom.getRootPomPath(baseDirPath);

         // Check if this is a maven project
        if (!pomPath.toFile().exists())
            throw new MojoExecutionException(baseDirPath + " does not contain a pom.xml file!");

        // create subfolder for submodule
        submodulePath = helper.createArtifactFolder(baseDirPath);
        // try to locate optional mixin root pom from template
        mixinPomPath = submodulePath.resolve("mixinpom.xml");
        // path to submodule's pom.xml
        submodulePomPath = submodulePath.resolve("pom.xml");
        //set featurerepoPath
        featurerepoPath = baseDirPath.resolve(getFeaturerepoPath());
        //set mixinFeaturerepoPath
        mixinFeaturerepoPath = submodulePath.resolve("mixinfeature.xml");
    }

    
    private void mergeMixinFeaturerepoIntoFeaturerepo() throws MojoExecutionException {
        if(mixinFeaturerepoPath.toFile().exists()){
            if(featurerepoPath.toFile().exists()){
                Document featurerepo = dom.parseDom(featurerepoPath);
                Document mixinFeaturerepo = dom.parseDom(mixinFeaturerepoPath);
                FeatureMerger.init().merge(mixinFeaturerepo, featurerepo);
                dom.writeDom(featurerepoPath, featurerepo);
                // delete mixinroot featurerepo
                helper.deleteFile(mixinFeaturerepoPath);
            } else {
                log.warn("Could not find featurerepo at " + featurerepoPath.toString() + "! No mixin features are merged.");
            }
        }
    }
    
    private Path getFeaturerepoPath() {
        String[] segments = _featurerepo.split("/");
        if(segments.length == 1){
            return Paths.get(segments[0]);
        } else {
            return Paths.get(segments[0], Arrays.copyOfRange(segments, 1, segments.length));
        }
    }

    private void validateTemplate() throws MojoExecutionException {
        if(!submodulePomPath.toFile().exists()){
            throw new MojoExecutionException("Submodule template does not contain a pom.xml at " + submodulePomPath.toString());
        }
    }

    private void mergeMixinPomIntoRootPom() throws MojoExecutionException {
        if(mixinPomPath.toFile().exists()) {
            Document rootPom = dom.parseDom(rootPomPath);
            Document mixin = dom.parseDom(mixinPomPath);
            POMMerger.init().merge(mixin, rootPom);
            dom.writeDom(rootPomPath, rootPom);
            // delete mixinroot pom
            helper.deleteFile(mixinPomPath);
        }
    }

    private void addParentToSubmodulePom() throws MojoExecutionException {
        Document submodulePom = dom.parseDom(submodulePomPath);
        pom.addParentToPom(pomPath, submodulePom);
        dom.writeDom(submodulePomPath, submodulePom);
    }

    private void addSubmoduleToPom() throws MojoExecutionException {
        Document pomDoc = dom.parseDom(pomPath);
        pom.addSubmoduleToPom(pomDoc);
        dom.writeDom(pomPath, pomDoc);
    }
}