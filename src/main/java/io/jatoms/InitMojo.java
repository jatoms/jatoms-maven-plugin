package io.jatoms;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "init", defaultPhase = LifecyclePhase.PROCESS_SOURCES, requiresProject = false)
public class InitMojo extends JatomsMojo {

    @Parameter(property = "jcoords", required = true)
    private String _coordinates;

    @Parameter(property = "jrepo", required = true)
    private String _repository;

    @Parameter(defaultValue = "jatoms.json", property = "jcontext", required = false)
    private String _context;

    public void execute() throws MojoExecutionException {
        init(_coordinates, _repository, _context);
        helper.fetchTemplate(baseDirPath);
        helper.parseDefaultContext(baseDirPath, context);
        helper.processTemplate(context, baseDirPath);
    }
}