package io.jatoms.util;

import java.util.List;
import java.util.function.BiFunction;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import io.jatoms.model.DOMMergeResult;

public class FeatureMerger {

     private DOMMerger merger;

    private FeatureMerger(){}
    
    public static FeatureMerger init(){
        FeatureMerger FeatureMerger = new FeatureMerger();
        DOMHelper dom = new DOMHelper();
        BiFunction<Node, Node, DOMMergeResult> findRepoWithSameText = (sourceNode, targetParent) -> {
            // only add node if it does not exist yet in the target
            List<Node> repositories = dom.childNodes(targetParent.getChildNodes(), "repository");
            for (Node repo : repositories) {
                if(repo.getTextContent().equals(sourceNode.getTextContent())){
                    return new DOMMergeResult(true, repo);
                }
            }
            return new DOMMergeResult(false, null);
        };

        BiFunction<Node, Node, DOMMergeResult> findFeatureWithSameName = (sourceNode, targetParent) -> {
            // only add node if it does not exist yet in the target
            List<Node> features = dom.childNodes(targetParent.getChildNodes(), "feature");
            String sourceFeatureName = sourceNode.getAttributes().getNamedItem("name").getTextContent();
            if(sourceFeatureName == null || sourceFeatureName.isBlank()){
                return new DOMMergeResult(false, null);
            }
            for (Node feature : features) {
                String targetFeatureName = feature.getAttributes().getNamedItem("name").getTextContent();
                if(targetFeatureName == null || targetFeatureName.isBlank()){
                    continue;
                } 
                if(sourceFeatureName.equals(targetFeatureName)){
                    return new DOMMergeResult(true, feature);
                }
            }
            return new DOMMergeResult(false, null);
        };

        FeatureMerger.merger =   DOMMerger.create()
        // set rootNode to features
        .root("features")
        // add rules for features elements
        .rule(findRepoWithSameText, "repository")
        .rule(findFeatureWithSameName, "feature");
      
        return FeatureMerger;
    }

    public Document merge(Document source, Document target){
        return merger.merge(source).into(target).doit();
    }
    
}