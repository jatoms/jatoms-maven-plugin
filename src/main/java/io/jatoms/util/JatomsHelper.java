package io.jatoms.util;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;

import io.jatoms.model.Jatoms;
import io.jatoms.model.JatomsContext;

public class JatomsHelper {

    private Log log;
    private ObjectMapper mapper = new ObjectMapper();
    private final VelocityEngine engine = new VelocityEngine();
    private Jatoms jatoms;

    public JatomsHelper(Log log, Jatoms jatoms) {
        this.log = log;
        this.jatoms = jatoms;
        // otherwise Jackson fails on properties it does not know
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        // otherwise velocity template processing fails on identifiers like "some-test"
        engine.setProperty("parser.allow_hyphen_in_identifiers", true);
        engine.init();
    }

    public void fetchTemplate(Path basedir) {
        log.info("\tCLONE TEMPLATE");
        // create temp directory to store repo in it
        log.info("\t\tCreating temporary directory for template");
        Path temp = createTempTemplateDirectory();
        // clone template into EMPTY temp directory (Git complains about non-empty
        // directories)
        cloneTemplate(jatoms.repository, temp.toFile());
        // copy template (without .git folder) to destination
        copyTemplate(temp, basedir);
        log.info("\t\tCleaning up temporary template directory");
        removeTempTemplateDirectory(temp);
    }

    public void parseDefaultContext(Path contextDir, JatomsContext context) throws MojoExecutionException {
        // only parse context if none has been set yet
        // JATOMS Object is always set so there has to be one object
        if(context.fields.size() == 1){
            // no other conetxt was set yet
            Path defaultContextPath = contextDir.resolve("default-jatoms.json");
            // if there is a default context
            if(defaultContextPath.toFile().exists()) {
                JatomsContext defaultContext = parseContext(defaultContextPath);
                defaultContext.fields.forEach((key, value) -> {
                    context.fields.put(key, value);
                });
                // delete it as it is not needed anymore
                deleteFile(defaultContextPath);
            }
        }
    }

     public JatomsContext parseContext(Path configPath) throws MojoExecutionException {
        JatomsContext config = null;
        try {
            log.info("\t\tParsing context file at " + configPath.toAbsolutePath().toString());
            String jsonString = Files.readString(configPath);
            config = mapper.readValue(jsonString, JatomsContext.class);
        } catch (IOException e) {
            log.debug(e);
            throw new MojoExecutionException("Was unable to parse context at " + configPath + " due to " + e.getMessage());
        }
        return config;
    }

    public void processTemplate(JatomsContext context, Path baseDir) {
        log.info("\tPROCESS TEMPLATE");
        log.info("\t\tSetting Velocity Context");
        log.debug("\t\t\tContext = " + toJson(context));
        VelocityContext vcontext = new VelocityContext(context.fields);
        log.info("\t\tTransforming Templates");
        transformTemplates(baseDir, vcontext);
    }

    private void transformTemplates(Path path, VelocityContext context) {
        try {
            Path baseDir = Paths.get(".").toAbsolutePath().normalize();
            transformTemplates(baseDir, path, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void transformTemplates(Path basedir, Path path, VelocityContext context) throws IOException {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path entry : stream) {
                if (Files.isDirectory(entry)) {
                    if (!entry.endsWith(Paths.get(".git")))
                        transformTemplates(basedir, entry, context);
                } else {
                    log.info("\t\t\tProcessing File: " + entry.toString());

                    Path pathRelative = basedir.relativize(entry);

                    // Usage of FileWriter somehow leads to empty files ?
                    // Therefore we use StringWriter and write the file ourselves
                    try (StringWriter writer = new StringWriter()) {
                        Template t = engine.getTemplate(pathRelative.toString());
                        t.merge(context, writer);
                        writer.flush();
                        // if we only overwrite, we get weird content, as it seems that the part of the
                        // original file that is longer than the newly written one is kept and therefore
                        // leads to invalid files
                        // Therefore we delete and create it new
                        Files.delete(entry);
                        Files.createFile(entry);
                        Files.writeString(entry, writer.toString(), StandardOpenOption.WRITE);
                    } catch (Exception e) {
                        log.warn("\t\t\tCould not merge template due to " + e);
                    }
                }
            }
        }
    }

    public Path createArtifactFolder(Path baseDirPath) throws MojoExecutionException {
        Path submoduleFolderPath = baseDirPath.resolve(jatoms.artifactId);
        log.info("\t\tCreating Artifact Folder at " + submoduleFolderPath.toString());
        try {
            return Files.createDirectories(submoduleFolderPath);
        } catch (IOException e) {
            log.debug(e);
            throw new MojoExecutionException("Could not create submodule folder at  " + submoduleFolderPath.toString());
        }
    }

    public void deleteFile(Path path) {
        try {
            log.info("\t\tDeleting File " + path.toString());
            Files.delete(path);
        } catch (IOException e) {
            log.debug(e);
            log.warn("Could not delete file " + path);
        }
    }

    private void cloneTemplate(String repository, File tempDir) {
        try {
            log.info("\t\tCloning template repository from " + jatoms.repository + " to " + tempDir.toString());
            Git.cloneRepository().setURI(repository).setDirectory(tempDir).call();
        } catch (InvalidRemoteException e) {
            e.printStackTrace();
        } catch (TransportException e) {
            e.printStackTrace();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }

    private Path createTempTemplateDirectory() {
        Path temp = null;
        try {
            temp = Files.createTempDirectory("jatoms-maven-plugin");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return temp;
    }

    private void copyTemplate(Path from, Path to) {
        try {
            log.info("\t\tCopying relevant template parts from " + from.toString() + " to " + to.toString());
            Path srcMainJava = Paths.get("src", "main", "java");
            Path srcMainTest = Paths.get("src", "main", "test");

            // Path fromSrcMainJava = from.resolve(srcMainJava);
            // Path fromSrcMainTest = from.resolve(srcMainTest);
            // Path toSrcMainJava = to.resolve(srcMainJava);
            // Path toSrcMainTest = to.resolve(srcMainTest);
            
            for (File child : from.toFile().listFiles()) {
                if (!".git".equals(child.getName())) {
                    Files.walk(child.toPath()).forEach(sourcePath -> {

                        // if sourcePath == src/main/java we need to add the package path between /java and the files below, same goes for testing
                        Path targetPath = to.resolve(from.relativize(sourcePath));
                        if((sourcePath.toString().contains(srcMainJava.toString()) && !sourcePath.endsWith(srcMainJava))
                        ||
                        (sourcePath.toString().contains(srcMainTest.toString()) && !sourcePath.endsWith(srcMainTest))){
                            // we need to insert the right package path
                            targetPath = calcTargetPath(from, to, sourcePath, targetPath);
                        }
                        try {
                            if(!Files.exists(targetPath.getParent()))
                                Files.createDirectories(targetPath.getParent());
                            Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException e) {
                            log.warn("\t\t\tFailed to copy " + sourcePath + " to " + targetPath);
                        }
                    });
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Path calcTargetPath(Path from, Path to, Path sourcePath, Path targetPath) {
        for (int i = 0; i < sourcePath.getNameCount(); i++) {
            Path segment = sourcePath.getName(i);
            if(segment.equals(Paths.get("src"))){
                // we found the start of the src/main/java part
                // determin if it is on root level or not
                Path fromSrcMainXXX = Paths.get("/").resolve(sourcePath.subpath(0, i + 3));
                if(i == (from.getNameCount())){
                    Path ending = fromSrcMainXXX.relativize(sourcePath);
                    Path fromSourceMainXXXRel = from.relativize(fromSrcMainXXX);
                    targetPath = to.resolve(fromSourceMainXXXRel).resolve(jatoms.packagePath).resolve(ending);
                } else {
                    // it lies deeper and we need to extract the part between root and beginning of source
                    Path subDirs = sourcePath.subpath(from.getNameCount(), i);
                    Path ending = fromSrcMainXXX.relativize(sourcePath);
                    Path fromSourceMainXXXRel = from.relativize(fromSrcMainXXX);
                    targetPath = to.resolve(fromSourceMainXXXRel).resolve(jatoms.packagePath).resolve(subDirs.resolve(ending));
                }

                break;
            }
        }
        return targetPath;
    }

    private void removeTempTemplateDirectory(Path dir) {
        try {
            Files.walk(dir).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String toJson(Object config) {
        try {
            return mapper.writeValueAsString(config);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    // public final String xslt = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
    //         + "<xsl:output indent=\"yes\"/>" + "<xsl:strip-space elements=\"*\"/>"
    //         + "<xsl:template match=\"@*|node()\">" + "<xsl:copy>" + "<xsl:apply-templates select=\"@*|node()\"/>"
    //         + "</xsl:copy>" + "</xsl:template>" + "</xsl:stylesheet>";
}