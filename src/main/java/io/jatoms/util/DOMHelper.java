package io.jatoms.util;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.maven.plugin.MojoExecutionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMHelper {

    public Element appendChild(Node parent, String name, String value, int nodeDepth, boolean newLineBefore, boolean newLineAfter) {
        Document doc = getDocument(parent);
        Element child = doc.createElement(name);
        
        String before = newLineBefore ? System.lineSeparator() + tabs(nodeDepth): tabs(nodeDepth);
        parent.appendChild(doc.createTextNode(before));
        parent.appendChild(child);

        if(value != null){
            child.appendChild(doc.createTextNode(value));
        }
       
        String after = newLineAfter ? System.lineSeparator() + tabs(nodeDepth): tabs(nodeDepth);
        parent.appendChild(doc.createTextNode(after));
        return child;
    }

    public Element insertBefore(Node parent, Node beforeNode, String tag, int nodeDepth, boolean newLineBefore, boolean newLineAfter){
        Document doc = getDocument(parent);
        Element newNode = doc.createElement(tag);

        String before = newLineBefore ? System.lineSeparator() + tabs(nodeDepth): tabs(nodeDepth);
        parent.appendChild(doc.createTextNode(before));

        parent.insertBefore(newNode, beforeNode);

        String after = newLineAfter ? System.lineSeparator() + tabs(nodeDepth): tabs(nodeDepth);
        parent.appendChild(doc.createTextNode(after));
        return newNode;
    }

    private String tabs(int nrOfTabs){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nrOfTabs; i++) {
            sb.append("\t");
        }
        return sb.toString();
    }

    private Document getDocument(Node node){
        Document doc = node.getOwnerDocument();
        if(doc != null){
            return doc;
        } else {
            return (Document) node;
        }
    }

    public Node getFirstChild(Node parent, String name) {
        return getFirstChild(parent, name, false, false);
    }

    public Node getFirstChild(Node parent, String name, boolean createIfAbsent, boolean newLineBefore) {
        for (int i = 0; i < parent.getChildNodes().getLength(); i++) {
            Node child = parent.getChildNodes().item(i);
            if (name.equals(child.getNodeName()))
                return child;
        }
        if (createIfAbsent) {
            return appendChild(parent, name, null, 1, newLineBefore, true);
        }
        return null;
    }

    public List<Node> childNodes(NodeList childNodes, String tag) {
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node child = childNodes.item(i);
            if(tag.equals(child.getNodeName())){
                nodes.add(childNodes.item(i));
            }
        }
        return nodes;
    }
    
    public Document parseDom(Path pomPath) throws MojoExecutionException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            File pomFile = pomPath.toFile();
            return db.parse(pomFile);
        } catch (Exception e) {
            throw new MojoExecutionException(
                    "Could not parse pom from " + pomPath.toString() + " due to " + e.getMessage());
        }
    }

     public void writeDom(Path domPath, Document dom) throws MojoExecutionException {
        TransformerFactory tf = TransformerFactory.newInstance();
        try {
            Transformer t = tf.newTransformer();
            t.transform(new DOMSource(dom), new StreamResult(domPath.toFile()));
        } catch (Exception e) {
            throw new MojoExecutionException(
                    "Could not write dom to " + domPath.toString() + " due to " + e.getMessage());
        }
    }
}