package io.jatoms.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import io.jatoms.model.Jatoms;

public class POMHelper {
    
    private Log log;
    private Jatoms jatoms;
    private DOMHelper dom;

    public POMHelper(Log log, DOMHelper dom, Jatoms jatoms) {
        this.log = log;
        this.dom = dom;
        this.jatoms = jatoms;
    }

    public void addParentToPom(Path pomPath, Document submodulePom) throws MojoExecutionException {
        Model project = parsePomToModel(pomPath);
        String parentVersion = getVersion(project);
        String parentGroupId = getGroupId(project);
        String parentArtifactId = getArtifactId(project);

        addParentToSubmodule(submodulePom, parentGroupId, parentArtifactId, parentVersion);
    }

    public void addSubmoduleToPom(Document pom) {
        log.info("\t\tAdding submodule " + jatoms.artifactId + " to pom at " + pom.getDocumentURI());
        Node project = dom.getFirstChild(pom, "project");
        Node modules = dom.getFirstChild(project, "modules", true, true);
        dom.appendChild(modules, "module", jatoms.artifactId, 1, false, true);
    }

    public Path getRootPomPath(Path baseDirPath) {
        Path currentPath = baseDirPath;
        Path lastPath = baseDirPath;
        while (currentPath.resolve("pom.xml").toFile().exists()) {
            lastPath = currentPath;
            currentPath = currentPath.getParent();
        }
        return lastPath.resolve("pom.xml");
    }

    private void addParentToSubmodule(Document pom, String group, String artifact, String version)
            throws MojoExecutionException {
        log.info("\t\tAdding parent " + group + ":" + artifact + ":" + version + " to pom at " + pom.getDocumentURI());

        Node project = dom.getFirstChild(pom, "project");
        Node childArtifactId = dom.getFirstChild(project, "artifactId");
        Node childGroupId = dom.getFirstChild(project, "groupId");

        if (childArtifactId != null || childGroupId != null) {
            Node before;
            if (childGroupId != null) {
                before = childGroupId;
            } else {
                before = childArtifactId;
            }

            Element parent = dom.insertBefore(project, before, "parent", 1, true, true);
            dom.appendChild(parent, "groupId", group, 2, true, false);
            dom.appendChild(parent, "artifactId", artifact, 2, true, false);
            dom.appendChild(parent, "version", version, 2, true, true);
            return;
        }
        throw new MojoExecutionException(
                "Could not add parent to submodule! (maybe pom didn't contain a 'modelVersion' tag?)");
    }

    private String getArtifactId(Model project) throws MojoExecutionException {
        String artifactId = project.getArtifactId();
        if (artifactId != null && !artifactId.isBlank()) {
            return artifactId;
        }
        throw new MojoExecutionException("Could not determine artifactId!");
    }

    private String getGroupId(Model project) throws MojoExecutionException {
        String groupId = project.getGroupId();
        if (groupId != null && !groupId.isBlank()) {
            return groupId;
        }
        Parent parent = project.getParent();
        if (parent != null) {
            groupId = parent.getGroupId();
            if (groupId != null && !groupId.isBlank()) {
                return groupId;
            }
        }
        throw new MojoExecutionException("Could not determine groupId!");
    }

    private String getVersion(Model project) throws MojoExecutionException {
        String version = project.getVersion();
        if (version != null) {
            if (version.matches("\\d+\\..*")) {
                return version;
            }
        }
        Parent parent = project.getParent();
        if (parent != null) {
            version = parent.getVersion();
            if (version != null) {
                if (version.matches("\\d+\\..*")) {
                    return version;
                }
            }
        }
        throw new MojoExecutionException("Could not determine version!");
    }

    public Model parsePomToModel(Path pomFile) throws MojoExecutionException {
        MavenXpp3Reader reader = new MavenXpp3Reader();
        try {
            return reader.read(new FileReader(pomFile.toFile()));
        } catch (FileNotFoundException e) {
            log.debug(e);
            throw new MojoExecutionException("No pom.xml found at " + pomFile.toString());
        } catch (IOException e) {
            log.debug(e);
            throw new MojoExecutionException("Error while reading " + pomFile.toString());
        } catch (XmlPullParserException e) {
            log.debug(e);
            throw new MojoExecutionException("Could not parse pom.xml at " + pomFile.toString());
        }
    }
}