package io.jatoms.util;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import io.jatoms.model.DOMMergeResult;

public class DOMMerger {
    // mapping from path to a function that determines if this node should be merged or not
    private Map<String, BiFunction<Node, Node, DOMMergeResult>> rules = new HashMap<>();
    private Document source;
    private Document target;
    private DOMHelper dom;
    private String rootNode;

    public static void main(String[] args) throws Exception {
        // How to merge two doms genercially?
        
        DOMHelper dom = new DOMHelper();
        Path res = Paths.get("src", "main", "resources");
        Path sourcePath = res.resolve("testSource.xml");
        Document sourceDoc = dom.parseDom(sourcePath);
        Path targetPath = res.resolve("testTarget.xml");
        Document targetDoc = dom.parseDom(targetPath);

        POMMerger.init().merge(sourceDoc, targetDoc);

        dom.writeDom(res.resolve("newTarget.xml"), targetDoc);
    }

    private DOMMerger() {
        this.dom = new DOMHelper();
    }

    public static DOMMerger create(){
        return new DOMMerger();
    }

    public DOMMerger rule(BiFunction<Node, Node, DOMMergeResult> rule, String first, String ... segments) {
        Path nodePath = Paths.get(first, segments);
        rules.put(nodePath.toString(), rule);
        return this;
    }

    public DOMMerger merge(Document sourceDoc) {
        this.source = sourceDoc;
        return this;
    }

    public DOMMerger into(Document targetDoc) {
        this.target = targetDoc;
        return this;
    }

    public DOMMerger root(String rootNode){
        this.rootNode = rootNode;
        return this;
    }

    public Document doit() {
        // traverse source in a depth first manner
        Path currentPath = Paths.get("");
        Node sourceProject = dom.getFirstChild(source, rootNode);
        Node targetProject = dom.getFirstChild(target, rootNode);
        for (int i = 0; i < sourceProject.getChildNodes().getLength(); i++) {
            Node child = sourceProject.getChildNodes().item(i);
            merge(child, targetProject, currentPath, source, target);
        }
        return target;
    }

    private void merge(Node sourceNode, Node targetParentNode, Path currentPath, Document source, Document target){
        if(sourceNode.getNodeType() == Node.COMMENT_NODE){
            // comments will not be copied from source to target
            return;
        }
        if(sourceNode.getNodeType() == Node.TEXT_NODE){
            // just copy the plain value
            Node copy = target.importNode(sourceNode, true);
            targetParentNode.appendChild(copy);
            // end of this subtree
            return;
        }
        String tag = sourceNode.getNodeName();

        // does the target parent node already have children of that type?
        if(tag != null && !tag.isBlank()) {
            // see if there is a rule for this child path
            currentPath = currentPath.resolve(tag);
            String key = currentPath.toString();
            Path parent = currentPath.getParent();
            String wildcardKey = null;
            if(parent != null){
                wildcardKey = currentPath.getParent().resolve("*").toString();
            }
            BiFunction<Node, Node, DOMMergeResult> rule = rules.get(key);

            // check for wildcard
            if(rule == null){
                rule = rules.get(wildcardKey);
            }
            // if there is a rule use it to determine the child in the target matching the source node (find or create)
            // and the traverse further
            Node targetNode = null;
            if(rule != null){
                // use rule to get target node
                DOMMergeResult result = rule.apply(sourceNode, targetParentNode);
                if(result.stop){
                    return;
                }
                targetNode = result.node;
            } 
            // fallback: create the node by copying it
            if(targetNode == null){
                // copy this node, but not its children
                targetNode = target.importNode(sourceNode, false);
                targetParentNode.appendChild(targetNode);
            }

            // go deeper into this subtree
            for (int i = 0; i < sourceNode.getChildNodes().getLength(); i++) {
                Node child = sourceNode.getChildNodes().item(i);
                merge(child, targetNode, currentPath, source, target);
            }

            // reduce path length 
            currentPath = currentPath.getParent();
        }
    }
}