package io.jatoms.util;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.BiFunction;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import io.jatoms.model.DOMMergeResult;

public class POMMerger {

    private DOMMerger merger;

    public static void main(String[] args) throws Exception {
        // How to merge two doms genercially?
        
        DOMHelper dom = new DOMHelper();
        Path res = Paths.get("src", "main", "resources");
        Path sourcePath = res.resolve("testSource.xml");
        Document sourceDoc = dom.parseDom(sourcePath);
        Path targetPath = res.resolve("testTarget.xml");
        Document targetDoc = dom.parseDom(targetPath);

        POMMerger.init().merge(sourceDoc, targetDoc);

        dom.writeDom(res.resolve("newTarget.xml"), targetDoc);
    }

    private POMMerger(){}
    
    public static POMMerger init(){
        POMMerger pomMerger = new POMMerger();
        DOMHelper dom = new DOMHelper();
        BiFunction<Node, Node, DOMMergeResult> findNodeOrReturnNull = (sourceNode, targetParent) -> {
            // only add node if it does not exist yet in the target
            return new DOMMergeResult(false, dom.getFirstChild(targetParent, sourceNode.getNodeName()));
        };

        BiFunction<Node, Node, DOMMergeResult> findDependencyGroupIdAndArtifactIdEqualOrReturnNull = (node, targetParent) -> {
            // nodes are considered equal if groupId, artifactId do match
            String srcGroupId = dom.getFirstChild(node, "groupId").getTextContent();
            String srcArtifactId = dom.getFirstChild(node, "artifactId").getTextContent();
          
            List<Node> deps = dom.childNodes(targetParent.getChildNodes(), "dependency");
            for (Node dep : deps) {
                String trgtGroupId = dom.getFirstChild(dep, "groupId").getTextContent();
                String trgtArtifactId = dom.getFirstChild(dep, "artifactId").getTextContent();
                
                if(srcGroupId.equals(trgtGroupId) && srcArtifactId.equals(trgtArtifactId) ){
                    return new DOMMergeResult(true, dep);
                }
            }
            return new DOMMergeResult(false, null);
        };

        BiFunction<Node, Node, DOMMergeResult> findPluginGroupIdAndArtifactIdEqualOrReturnNull = (node, targetParent) -> {
            // nodes are considered equal if groupId, artifactId do match
            String srcGroupId = dom.getFirstChild(node, "groupId").getTextContent();
            String srcArtifactId = dom.getFirstChild(node, "artifactId").getTextContent();
          
            List<Node> deps = dom.childNodes(targetParent.getChildNodes(), "plugin");
            for (Node dep : deps) {
                String trgtGroupId = dom.getFirstChild(dep, "groupId").getTextContent();
                String trgtArtifactId = dom.getFirstChild(dep, "artifactId").getTextContent();
                
                if(srcGroupId.equals(trgtGroupId) && srcArtifactId.equals(trgtArtifactId) ){
                    return new DOMMergeResult(true, dep);
                }
            }
            return new DOMMergeResult(false, null);
        };
        pomMerger.merger =   DOMMerger.create()
        // set rootNode to project
        .root("project")
        // add rules for pom elements
        .rule(findNodeOrReturnNull, "dependencies")
        .rule(findNodeOrReturnNull, "dependencyManagement")
        .rule(findNodeOrReturnNull, "dependencyManagement", "dependencies")
        .rule(findNodeOrReturnNull, "plugins")
        .rule(findNodeOrReturnNull, "properties")
        .rule(findNodeOrReturnNull, "contributors")
        .rule(findNodeOrReturnNull, "build")
        .rule(findNodeOrReturnNull, "build", "pluginManagement")
        .rule(findNodeOrReturnNull, "build", "pluginManagement", "plugins")

        // add rules for dependencies/plugins/etc
        .rule((node, targetParent) -> {
            // dependencies are considered equal if groupId, artifactId and version do match
            String srcGroupId = dom.getFirstChild(node, "groupId").getTextContent();
            String srcArtifactId = dom.getFirstChild(node, "artifactId").getTextContent();
            String srcVersion = dom.getFirstChild(node, "version").getTextContent();
            List<Node> deps = dom.childNodes(targetParent.getChildNodes(), "dependency");
            for (Node dep : deps) {
                String trgtGroupId = dom.getFirstChild(dep, "groupId").getTextContent();
                String trgtArtifactId = dom.getFirstChild(dep, "artifactId").getTextContent();
                String trgtVersion = dom.getFirstChild(dep, "version").getTextContent();
                if(srcGroupId.equals(trgtGroupId) && srcArtifactId.equals(trgtArtifactId) && srcVersion.equals(trgtVersion)){
                    return new DOMMergeResult(true, dep);
                }
            }
            return new DOMMergeResult(false, null);

        }, "dependencies", "dependency")
        .rule(findDependencyGroupIdAndArtifactIdEqualOrReturnNull, "dependencyManagement", "dependencies", "dependency")
        .rule(findPluginGroupIdAndArtifactIdEqualOrReturnNull, "build", "pluginManagement", "plugins", "plugin")
        .rule(findPluginGroupIdAndArtifactIdEqualOrReturnNull, "build", "plugins", "plugin")
        .rule((node, targetParent) -> {
            // do not add property if there is already one with the same name
            Node prop = dom.getFirstChild(targetParent, node.getNodeName());
            return (prop == null) ? new DOMMergeResult(false, null) : new DOMMergeResult(true, prop);
        }, "properties", "*");
        return pomMerger;
    }

    public Document merge(Document source, Document target){
        return merger.merge(source).into(target).doit();
    }
}