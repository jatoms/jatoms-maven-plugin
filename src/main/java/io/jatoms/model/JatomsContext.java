package io.jatoms.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

public class JatomsContext {

    public Map<String, Object> fields = new HashMap<>();

    // Capture all other fields that Jackson do not match other members
    @JsonAnyGetter
    public Map<String, Object> otherFields() {
        return fields;
    }

    @JsonAnySetter
    public void setOtherField(String name, Object value) {
        fields.put(name, value);
    }
    
}