package io.jatoms.model;

import org.w3c.dom.Node;

public class DOMMergeResult {

    public boolean stop;
    public Node node;

    /**
     * A result to calculated by a DOM Merge rule
     * @param stop if the traversal of the tree should stop at this node and go back up or go deeper into the tree
     * @param node the node to be used for further traversal or null if the traverser should create it (e.g., by copying it) if not advised otherwise by "stop"
     */
    public DOMMergeResult(boolean stop, Node node) {
        this.stop = stop;
        this.node= node;
    }
    
}