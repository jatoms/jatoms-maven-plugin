package io.jatoms.model;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class Jatoms {
    public String repository;
    public String coordinates;
    public String groupId;
    public String artifactId;
    public String version;

    public Path packagePath;
    public String packageString;

    public Map<String, Object> toMap () {
        Map<String, Object> map = new HashMap<>();
        map.put("repository", repository);
        map.put("coordinates", coordinates);
        map.put("groupId", groupId);
        map.put("artifactId", artifactId);
        map.put("version", version);
        map.put("packagePath", packagePath.toString());
        map.put("package", packageString);
        return map;
    }
}