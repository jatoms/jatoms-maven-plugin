# Jatoms Maven Plugin

This is the Jatoms Maven Plugin used for easier multi-module Maven Java projects, or:

MAVEN ARCHETYPES ON STEROIDS ;)

## Quickstart
* in your Maven settings.xml (usually at ~/.m2/settings.xml) add the following:
```xml
<profiles>
    <profile>
        <id>jatoms</id>
        <pluginRepositories>
            <pluginRepository>
                <id>jatoms-maven-plugin</id>
                <name>Jatoms Maven Plugin Repository</name>
                <releases>
                    <enabled>true</enabled>
                </releases>
                <snapshots>
                    <enabled>true</enabled>
                </snapshots>
                <url>https://gitlab.com/api/v4/projects/23468686/packages/maven</url>
            </pluginRepository>
        </pluginRepositories>
    </profile>
</profiles>

<pluginGroups>
    <pluginGroup>io.jatoms</pluginGroup>
</pluginGroups>

<activeProfiles>
    <activeProfile>jatoms</activeProfile>
</activeProfiles>
```
* create a file `jatoms.json` with the following contents:
```json
{
    "karafinit": {
        "version": "4.3.0",
        "slug": "my-project-slug"
    }
}

``` 
* Type `mvn jatoms:init -Djrepo=https://gitlab.com/jatoms/jatoms-archetype-karaf.git -Djcoords=<your groupId>:<your artifactId>:<version>` in the folder you created the `jatoms.json` in
* The template is cloned and processed for you
* Now you have a working Gitpod setup for your first Apache Karaf based project (at least if you used a real slug ;) )
* If you want to add a submodule to your project you can now type `mvn jatoms:submodule -Djrepo=<path to your submodule template repo> -Djcoords=<your groupId>:<your artifactId>:<version>` . Currently we have two working examples:
    * A simple OSGi service submodule: https://gitlab.com/jatoms/karaf-submodule-service
    * A copmlex REST service submodule with integrated Swagger API: https://gitlab.com/jatoms/jatoms-submodule-rest

## What this plugin provides
This plugin provides a simple mechanism to create opinionated multi-module maven Java project templates.
This plugin is opinionated, as it assumes a certain folder/project layout as depicted below.
```
project root/
├── api/
│   ├── src/
│   │   └── main/
│   |       └── java/
│   |           └── Stuff.java
|   └── pom.xml
├── impl/
│   ├── src/
│   |   └── main/
│   |       └── java/
│   |           └── OtherStuff.java   
|   └── pom.xml
└── pom.xml
```
Aside from the folder layout the plugin assumes that the developer intends to manage all properties, dependencies and plugins in the root pom.xml and the submodule pom.xml only usees these properties/dependencies/plugins.
In order to see such a project layout in action you can have a look at the [Apache Karaf](https://github.com/apache/karaf) project.

For such types of projects this plugin offers several advantages over good old maven archetypes:
* Easier onboarding: Templates are just plain old GIT repositories 
    * No jar has to be published somewhere
    * Just create a new (public) GitHub/GitLab repo, put your template files there and you are good to go.
* Ability for submodule templates to merge properties/dependencies/plugins into a preexisting root pom.xml 
    * Just define a `mixinpom.xml` in the root folder of your template and all properties/dependencies/plugins you define there are merged into the proerties/dependencyManagement/pluginManagement section of the root pom.xml

### Goals 
The plugin supports two goals: `init` for an initial multi-module project setup and `submodule` for adding submodules at an arbitrary depth into the multi-module project while still being able to alter the root pom via the `mixinpom.xml`. 

#### init
A `init` project template can be invoked via `mvn jatoms:init -Djrepo="https://gitlab.com/<yourproject>" -Djcoords=<your groupId>:<your artifactId>:<version>` 
* `-Djrepo`: the URL of your Git repository where all the template files are stored, e.g., https://gitlab.com/jatoms/jatoms-archetype-karaf.git
* `-DJcoords`: the Maven coordinates you want to use for your project, e.g., io.jatoms:jatoms-test:1.0.0-SNAPSHOT

This command will 
* clone the repository into the folder from where the command was invoked
* process all files as Apache Velocity templates

#### submodule
A `submodule` project template can be invoked likewise via `mvn jatoms:submodule -Djrepo="https://gitlab.com/<yourproject>" -Djcoords=<your groupId>:<your artifactId>:<version>`
The parameters are the same, but processing is different.

This command assumes to be invoked from within a directory that already conatins a valid pom.xml file and then will
* create a subfolder named like the artifactId provided in the `Djcoords` parameter
* clone the repository contents into this folder
* create a `submodule` entry within the pom.xml of the folder the command was invoked from 
* create a `parent` entry in the pom.xml of the freshly cloned submodule
* search for a `mixinpom.xml` file within the cloned template files and will try to merge all property/dependencyManagement dependency/pluginManagement plugin entries into the root pom.xml file of the multi module project.
* delete the `mixinpom.xml`
* process the rest of the cloned template files as Apache Velocity templates 


### Templates 
Templates are written as Apache Velocity Templates just as you would do for a normal Apache Maven Archetype.
The files have to be stored in a public Git repository that can be accessed without authentication.

### How to write a template 
First youneed to create a new git repository somewhere, e.g., GitHub or GitLab.
Then in this git repository you can create all the files you need your template to have copied.
However there are some rules you have to comply to:
1) if your template contains a `mixinpom.xml` at the root level, this file will not be copied, but merged with the root pom of the project your template is copied into 
2) if your template contains sourcecode, then this code has to reside within a `src/main/java` folder at an arbitrary depth. You don't need to create groupId/artifactId related packages, as those will be generated for you.

Let's see this for an example template. (The complete template with similar naming can also be seen here: https://gitlab.com/jatoms/karaf-submodule-service)

Assume we want to create a template for an OSGi service. 
A service in OSGi usually is split into two subprojects, i.e., one for the interface and one for the implementation, so a template project's folder layout for such a template would look like this:
```
template/
├── api/
│   ├── src/
│   │   └── main/
│   |       └── java/
│   |           └── MyInterface.java
|   └── pom.xml
├── impl/
│   ├── src/
│   |   └── main/
│   |       └── java/
│   |           └── MyImpl.java   
|   └── pom.xml
├── mixinpom.xml
└── pom.xml
```

The `pom.xml` at root level would contain the normal pom stuff for this project with some template parts, e.g., it would look like this:
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>${JATOMS.groupId}</groupId>
    <artifactId>${JATOMS.artifactId}</artifactId>
    <version>${JATOMS.version}</version>
    <packaging>pom</packaging>

    <modules>
        <module>api</module>
        <module>impl</module>
    </modules>

</project>
```

The mixinpom.xml would contain the stuff that should be added into the root pom of the project this template is inserted to, so it would contain the maven-bundle-plugin, that is used to create bundles from your subprojects.

```xml 
<?xml version="1.0" encoding="UTF-8" ?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <properties>
        <bnd.version>5.0.0</bnd.version>
        <maven-bundle-plugin.version>5.1.1</maven-bundle-plugin.version>
        <osgi.version>7.0.0</osgi.version>
    </properties>

    <!-- Needed if not defined by the root project already -->
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>${maven-bundle-plugin.version}</version>
                    <extensions>true</extensions>
                    <dependencies>
                        <dependency>
                            <groupId>biz.aQute.bnd</groupId>
                            <artifactId>biz.aQute.bndlib</artifactId>
                            <version>${bnd.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <instructions>
                            <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
                            <Bundle-Version>${project.version}</Bundle-Version>
                            <Import-Package>*</Import-Package>
                            <_dsannotations>*</_dsannotations>
                        </instructions>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>
```

The pom.xmls at subproject level will contain the stuff needed in order to build the respective subproject, e.g., the one for the impl subproject would look like this:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>${JATOMS.groupId}</groupId>
        <artifactId>${JATOMS.artifactId}</artifactId>
        <version>${JATOMS.version}</version>
    </parent>

    <artifactId>${JATOMS.artifactId}-impl</artifactId>
    <packaging>bundle</packaging>

    <dependencies>
        <dependency>
            <groupId>${JATOMS.groupId}</groupId>
            <artifactId>${JATOMS.artifactId}-api</artifactId>
            <version>${project.version}</version>
        </dependency>
        <!-- for @Component annotation -->
        <dependency>
            <groupId>org.osgi</groupId>
            <artifactId>osgi.cmpn</artifactId>
            <version>${osgi.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-bundle-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>
```
It contains the dependencies needed for the impl bundle to be compiled (i.e., `org.osgi:osgi.cmpn`). The version is expressed by a property that will be merged from the mixinpom.xml into the root pom. It also uses the maven-bundle-plugin, that has been added to the project's root via the mixinpom.xml.

Finally the actual code could look like the following:

```java 
package ${JATOMS.package}.basic;

import ${JATOMS.package}.api.MyInterface;
import org.osgi.service.component.annotations.Component;

@Component
public class MyImpl implements MyInterface {
    @Override
    public String foo(String toGreet) {
        return "Foo " + toGreet;
    }
}
```
The `${JATOMS.package}` parts are valid Velocity template strings that will be replaced with the calculated package for the template, i.e., the groupId + artifactId given via the `-Djcoords` parameter during plugin invocation, e.g., if your groupId is "io.jatoms" and your artifactId is "my-project", then the package would be "io.jatoms.my.project".

Now you will have noticed, that there is no path below `src/main/java` that would reflect this package. This is because for all files and folders below a `src/main/java` or `src/main/test` path this package is calculated and inserted automatically by the plugin.
For example for the above template the final processed template folder structure for the coordinates `io.jatoms:service:...` would look like this:

```
projectfolder/
├── api/
│   ├── src/
│   │   └── main/
│   |       └── java/
|   |           └── io/
│   │               └── jatoms/
│   |                   └── service/
|   |                       └── api/
│   |                           └── MyInterface.java
|   └── pom.xml
├── impl/
│   ├── src/
│   │   └── main/
│   |       └── java/
|   |           └── io/
│   │               └── jatoms/
│   |                   └── service/
|   |                       └── impl/
│   |                           └── MyImpl.java
|   └── pom.xml
├── mixinpom.xml
└── pom.xml
```
The package path is automatically calculated and appended with an optional path from the root to the respetive subproject, e.g, api or impl in this case.

#### Template Processing Context 
In Velocity templates usually you have template expressions like this: `${project.version}` which are translated during processing into something meaningful like `1.0.0-SNAPSHOT`
In order to be able to replace such expressions, Velocity has to be provided with a Context Object, e.g., a HashMap that contains the keys/values that are used to replace the template expressions.
For our example we would need a HashMap that has one entry `project` which as value has another HashMap with the entry `version` that has the value `1.0.0-SNAPSHOT`
```
HashMap
|-- "project" -> HashMap 
                |-- "version" -> "1.0.0-SNAPSHOT"
```

In order to provide all the context you need for your respective template you can put a `jatoms.json` file into the folder from where the `mvn jatoms:init/submodule` command was invoked.
In this file you can define all the properties you need for processing your templates in the right way, e.g., 
```json
// context object
{
    "jatoms": {
        "karaf": {
            "version": "4.3.0"
        }
    }
}
```
which then can be used in your template like this:
```xml
<dependency>
    <groupId>org.apache.karaf</groupId>
    <artifactId>karaf-bom</artifactId>
    <version>${jatoms.karaf.version}</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

##### JATOMS Context Object
The context will always implicitly contain one additional element `JATOMS` which should be treated like a reserved keyword.
The `JATOMS`element will always be added to the context object and therefore the context object always will contain an entry like this:
```json
// context object
{
    // implicitly set by the plugin
    "JATOMS": {
        "repository":"<value of -Djrepo>, e.g., https://gitlab.com/jatoms/jatoms-archetype-karaf",
        "coordinates":"<value of -Djcoords>, e.g., io.jatoms:init-template:1.0.0-SNAPSHOT",
        "groupId":"<groupId parsed from coordinates>, e.g., io.jatoms",
        "artifactId":"<artifactId parsed from coordinates>, e.g., init-template",
        "version":"<version parsed from coordinates>, e.g., 1.0.0-SNAPSHOT",
        "package":"<parsed from groupId and artifactId>, e.g., io.jatoms.init.template"
    }
    // ... your properties
}
```
You also can use this properties of course in your templates if you like, like this:
```xml
<groupId>${JATOMS.groupId}</groupId>
<artifactId>${JATOMS.artifactId}</artifactId>
<version>${JATOMS.version}</version>
```

### Apache Karaf specific processing 
The jatoms-maven-plugin is a general usable archetype plugin, but it comes with an additional feature that makes it extra usable for Apache Karaf project setups.
In addition to the `mixinpom.xml` which is merged with a root pom, it also processes `mixinfeature.xml` files at root level and merges those with a featurerepo's feature.xml file, which ca either be given via the `-Djfeature` parameter or it is assumed to be at `<projectroot>/featurerepo/src/main/feature/feature.xml`

This means that you are able to create templates, that also create an additional feature in the project's `feature.xml` that contains already all needed bundles/features/configs, that your type of template project needs to be able to run in an Apache Karf container.

What does this look like for the example OSGi service from above? Well, our service would always need the api/impl bundle to be runnable and also the `scr` feature of Apache Karaf, so that our service is actually instantiated.
The corresponding `mixinfeature.xml` thereofre would look like this:
```xml 
<?xml version="1.0" encoding="UTF-8"?>
<features name="${project.artifactId}-${project.version}" xmlns="http://karaf.apache.org/xmlns/features/v1.6.0">

    <feature name="my-service" description="A simple service with api and impl" version="${project.version}">
        <feature>scr</feature>
                
        <bundle>mvn:${JATOMS.projectId}/${JATOMS.artifactId}-api/${project.version}</bundle>
        <bundle>mvn:${JATOMS.projectId}/${JATOMS.artifactId}-impl/${project.version}</bundle>
    </feature>

</features>
```

The feature declared within would be merged with preexisting features in the repsective feature.xml of the project.
This is an easy way to create templates for complete features, like REST services or DB services, which usually come with a lot of dependencies that you usually would have to gather by yourself.
You can also see this for a real template here: https://gitlab.com/jatoms/karaf-submodule-service
